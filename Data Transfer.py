#!/usr/bin/env python
# coding: utf-8

# In[1]:


try:
    
    print("Writing Data to Dashboard....\n")
    import pandas as pd
    import os
    loginname=os.getlogin()
#    below is the code for the main file
    path1="C:/Users/"+loginname+"/Documents/Dashboard/main.xlsx"
#   below is the code for the changedirectory
    path2="C:/Users/"+loginname+"/Documents/Dashboard"

    with pd.ExcelFile(f"{path1}") as wt:
        df=pd.read_excel(wt,sheet_name="MASTER SHEET",skiprows=2)
    
    
    
    os.chdir(f"{path2}")
    os.startfile("Dashboard.xlsm")
    import time
    time.sleep(2)
    import xlwings as xw
    
    Book=xw.books("Dashboard.xlsm")
    
    sheet=Book.sheets("Data")
    
    print("\nRefreshing Data.....")
    
    sheet.range("a2:ac1048576").value=""
    
    # print("\nWriting Data...")
    
    sheet.range(1,1).value=df
    
    uniqueiddf=pd.DataFrame(df["Reg ID"].unique())
    
    uniqueiddf=uniqueiddf.sort_values(by=0,ascending=False)

    sheet.range(1047548,16371).value=uniqueiddf

# Below is the File modified date and time code

    modtime=os.path.getmtime("main.xlsx")
    
    import time
    
    motime=time.ctime(modtime)
    
    sheet.range(1048554,16378).value=motime
    print("\nRefreshing Data Completed")
    input("\nWriting data to HAMAR Energy Dashboard completed :")


except Exception as e:
    
    print(e)
    input("Error in the Data Transfer from main to Dashboard....")







# In[65]:


# df

